/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 *
 * Created by Yury Bondarenko
 * contact email this.brack@gmail.com
 *
 */

/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 *
 * Created by Yury Bondarenko
 * contact email this.brack@gmail.com
 *
 */

package org.swarl.brack11.location.model;

import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Yury Bondarenko on 2017-03-29.
 * <p>
 * Owner: Yury Bondarenko
 * Website: http://www.swarl.org
 * Email: this.brack@gmail.com
 */
public class Coordinate {
    private String coordinate;
    private static double lat,lon;

    private static final Pattern LAT_RE = Pattern.compile("([NS])([0-9][0-9][0-9])[ ]([0-9][0-9](\\.[0-9][0-9][0-9]))$", Pattern.CASE_INSENSITIVE);
    private static final Pattern LON_RE = Pattern.compile("([EW])([0-9][0-9][0-9])[ ]([0-9][0-9](\\.[0-9][0-9][0-9]))$", Pattern.CASE_INSENSITIVE);

    public Coordinate(String prefix, Double value) {
        switch (prefix.toLowerCase()) {
            case "lat":
                coordinate = latToDM(value);
                lat = value;
                break;
            case "lon":
                coordinate = lonToDM(value);
                lon = value;
                break;
            default:
                throw new IllegalArgumentException("The name of coordinate must be lower case 'lat' or 'lon'");
        }
    }

    public Coordinate(String prefix, String value) {
        switch (prefix.toLowerCase()) {
            case "lat":
                lat = dmToLat(value);
                coordinate = latToDM(lat);
                break;
            case "lon":
                lon = dmToLon(value);
                coordinate = lonToDM(lon);
                break;

            default:
                throw new IllegalArgumentException("Prefix must be 'lat' or 'lon'");
        }
    }

    private static String getLongitudePrefix(Double lon) {
        return lon >= 0.0 ? "E" : "W";
    }

    private static String getLatitudePrefix(Double lat) {
        return lat >= 0.0 ? "N" : "S";
    }

    private void latVSlon(String coordinate) {
        char prefix = coordinate.charAt(0);
        switch (prefix) {
            case 'N':
            case 'S':
                this.coordinate = coordinate;
                lat = dmToLat(coordinate);
                break;
            case 'E':
            case 'W':
                this.coordinate = coordinate;
                lon = dmToLon(coordinate);
                break;
            default:
                this.coordinate = null;
                throw new IllegalArgumentException("Coordinate expected to start with one of the prefixes 'E' 'W' or 'N' 'S'");
        }
    }

    public String getCoordinate() {
        return coordinate;
    }

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }

    public static Double toDecimal(Double coordinate) {
        return coordinate / 180;
    }

    public static Double toDecimal(String coordinate) {
        Double c = Double.parseDouble(coordinate);
        return c / 180;
    }

    public static String lonToDM(double lon) {
        String prefix = getLongitudePrefix(lon);
        lon = Math.abs(lon);
        int deg = (int) lon;
        double min = 60.0 * (lon - (int)lon);

        return String.format(Locale.ROOT,"%s%03d %06.3f", prefix, deg, min);
    }

    public static String latToDM(double lat) {
        String prefix = getLatitudePrefix(lat);
        lat = Math.abs(lat);
        int deg = (int) lat;
        double min = 60.0 * (lat - (int)lat);

        return String.format(Locale.ROOT,"%s%03d %06.3f", prefix, deg, min);
    }

    public static double dmToLat(String string) {
        Matcher matcher = LAT_RE.matcher(string);
        if (matcher.matches()) {
            String prefix = matcher.group(1);
            int deg = Integer.parseInt(matcher.group(2));
            double min = Double.parseDouble(matcher.group(3));
            return (prefix.equalsIgnoreCase("N") ? 1 : -1) * (deg + (min / 60.0));
        } else if (isNumeric(string)) {
            Double lat = Double.parseDouble(string);
            return lat;
        } else {
            throw new IllegalArgumentException("Bad latitude format " + string);
        }
    }

    public static double dmToLon(String string) {
        Matcher matcher = LON_RE.matcher(string);
        if (matcher.matches()) {
            String prefix = matcher.group(1);
            int deg = Integer.parseInt(matcher.group(2));
            double min = Double.parseDouble(matcher.group(3));
            return (prefix.equalsIgnoreCase("E") ? 1 : -1) * (deg + (min / 60.0));
        } else if (isNumeric(string)) {
            Double lon = Double.parseDouble(string);
            return lon;
        } else {
            throw new IllegalArgumentException("Bad longitude format " + string);
        }
    }

    @Override
    public String toString() {
        return String.valueOf(coordinate);
    }


    public static boolean isNumeric(String str)
    {
        try
        {
            double d = Double.parseDouble(str);
        }
        catch(NumberFormatException nfe)
        {
            return false;
        }
        return true;
    }
}
