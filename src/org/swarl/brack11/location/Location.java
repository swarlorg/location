package org.swarl.brack11.location;

import org.swarl.brack11.location.model.Coordinate;

import java.util.regex.Pattern;

/**
 * File is part of the project Location
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 * <p>
 * File is part of the project Location
 * Created by Yury Bondarenko on 2017-06-11.
 * contact email this.brack@gmail.com
 */
public class Location {
    private static Coordinate lon, lat;
    private final Pattern LAT_RE = Pattern.compile("([NS])([0-9][0-9][0-9])[ ]([0-9][0-9](\\.[0-9][0-9][0-9]))$", Pattern.CASE_INSENSITIVE);
    private final Pattern LON_RE = Pattern.compile("([EW])([0-9][0-9][0-9])[ ]([0-9][0-9](\\.[0-9][0-9][0-9]))$", Pattern.CASE_INSENSITIVE);


    public Location(double lon, double lat) {
        this.lon = new Coordinate("lon", lon);
        this.lat = new Coordinate("lat", lat);
    }



    public static String latLonToGridSquare(float lat, float lon, int precision) throws Exception{
        String locator = "";
        float adjLat,adjLon;
        char GLat,GLon;
        String nLat,nLon;
        char gLat,gLon;
        float rLat,rLon;
        String U = "ABCDEFGHIJKLMNOPQRSTUVWX";
        String L = U.toLowerCase();

        // support Chris Veness 2002-2012 LatLon library and
        // other objects with lat/lon properties
        // properties could be getter functions, numbers, or strings


        if (Float.isNaN(lat)) throw new Exception("lat is NaN");
        if (Float.isNaN(lon)) throw new Exception("lon is NaN");
        if (Math.abs(lat) == 90.0) throw new Exception("grid squares invalid at North/South poles");
        if (Math.abs(lat) > 90) throw new Exception("invalid latitude: "+lat);
        if (Math.abs(lon) > 180) throw new Exception("invalid longitude: "+lon);


        adjLat = lat + 90;
        adjLon = lon + 180;
        GLat = U.charAt((int) (adjLat/10));
        GLon = U.charAt((int) (adjLon/20));
        nLat = ""+(int)(adjLat % 10);
        nLon = ""+(int)((adjLon/2) % 10);
        rLat = (adjLat - (int)(adjLat)) * 60;
        rLon = (adjLon - 2*(int)(adjLon/2)) *60;
        if (precision > 4) {
            gLat = L.charAt((int) (rLat / 2.5));
            gLon = L.charAt((int) (rLon / 5));
            locator = "" + GLon + GLat + nLon + nLat + gLon + gLat;
        } else {
            locator = "" + GLon + GLat + nLon + nLat;
        }
        return locator;
    }


    /**
     * 18 x 18 fields, First letters * 10 for latitude and * 20 for longitude
     * 10 x 10 squares, so digits are 1 deg of lat and 2 deg of long
     * Final 2 letters are 2.5 mins of lat and 5 mins of long
     *
     * We add a half to the last two letters location so that we get the middle of the square and not the corner
     * @param grid
     * @throws Exception
     */
    private static void gridSquareToLatLon(String grid) throws Exception {
        if (grid.length() != 6) throw new Exception("Maidenhead locator needs to be 6 characters");
        grid = grid.toUpperCase();
        char gLon, gLat, GLat, GLon;
        char nLat, nLon;
        String U = "ABCDEFGHIJKLMNOPQRSTUVWX";
        String L = U.toLowerCase();

        GLon = grid.charAt(0);
        GLat = grid.charAt(1);
        nLon = grid.charAt(2);
        nLat = grid.charAt(3);
        gLon = grid.charAt(4);
        gLat = grid.charAt(5);

        // Long
        float lon = U.indexOf(GLon);
        lon = lon * 20;
        lon = lon + Character.getNumericValue(nLon) * 2;
        lon = lon + (((float)U.indexOf(gLon)+0.5f) * 5f )/ 60f;


        // move from adjusted to standard longitude where West negative
        lon = lon - 180;
        if (Float.isNaN(lon)) throw new Exception ("lon is NaN");
        if (Math.abs(lon) > 180) throw new Exception ("invalid longitude: "+lon);
        Location.lon = new Coordinate("lon",(double) lon);

        // Lat
        float lat = U.indexOf(GLat);
        lat = lat * 10;
        lat = lat + Character.getNumericValue(nLat);
        lat = lat + (((float)U.indexOf(gLat)+0.5f) * 2.5f )/ 60f;

        lat = lat - 90;
        if (Float.isNaN(lat)) throw new Exception("lat is NaN");
        if (Math.abs(lat) == 90.0) throw new Exception ("grid squares invalid at N/S poles");
        if (Math.abs(lat) > 90) throw new Exception ("invalid latitude: "+lat);
        Location.lat = new Coordinate("lat",(double) lat);

    }
}
